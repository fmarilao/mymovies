import axios from "axios";
import { useState } from "react";
import "./PersonCard.module.css";
import Modal from 'react-modal';
import AddPeopleForm from "../AddPeopleForm/AddPeopleForm";

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    color: 'black',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    background: '#282c34',
  },
};

export default function PersonCard({ person, onUpdate }) {
  const [modalIsOpen, setIsOpen] = useState(false)

  const deletePerson = async () => {
    try {
      const res = await axios.delete(`https://hellowworldapi.azurewebsites.net/Person/${person.id}`)
      if (res && res.status === 200) {
        console.log('RESP', res)
        onUpdate()
      }
    } catch (error) {
        console.error(error)
    }
  }

  return (
    <div className="Card">
      <span>Id: {person.id}</span>
      <p>Nombre: {person.name}</p>
      <p>Edad: {person.edad}</p>
      <p>Estatura:{person.estatura}</p>
      <p>Mensaje:{person.speak}</p>
      <button type="button" onClick={() => setIsOpen(true)}>Editar</button>
      <button type="button" onClick={() => deletePerson()}>Borrar</button>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={() => setIsOpen(false)}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <button onClick={() => setIsOpen(false)}>Close</button>
        <AddPeopleForm
          isEditForm={true}
          person={person}
          closeModal={() => setIsOpen(false)}
          onUpdate={onUpdate}
        />
      </Modal>
    </div>
  );
}
