import React from 'react'
import './PageNotFound.css'
const PageNotFound = () => {
  return (
    <div className="Container">
      Error 404 - page not found
    </div>
  )
}

export default PageNotFound