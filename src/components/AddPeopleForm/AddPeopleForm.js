import { ErrorMessage, Field, Form, Formik } from 'formik';
import React from 'react';
import './AddPeopleForm.css';
import * as Yup from 'yup';
import axios from 'axios';

const AddPeopleForm = ({ isEditForm, person, closeModal, onUpdate }) => {

  const editPerson = async (values) => {
    try {
      const res = await axios.put(`https://hellowworldapi.azurewebsites.net/Person/${person.id}`, values)
      if (res && res.status === 200) {
        // volver a cargar la lista
        onUpdate()
      }
    } catch (error) {
        console.error(error)
    }
  }
  
  const sendPerson = async (values) => {
    try {
      const res = await axios.post('https://hellowworldapi.azurewebsites.net/Person', values)
      if (res && res.status === 200) {
        console.log('RESP DEL POST', res)
      }
    } catch (error) {
        console.error(error)
    }
  }

  return (
    <div className="Container">
      <h3>{isEditForm ? 'Editar persona' : 'Cargar persona'}</h3>  
      <Formik
        initialValues={{
          speak: person?.speak || '',
          name: person?.name || '',
          edad: person?.edad || '',
          estatura: person?.estatura || '',
          moviesWatched: [],
        }}
        onSubmit={async (values, { resetForm }) => {
          if (isEditForm) {
            // codigo que se ejecuta si estoy editando
            editPerson(values)
            closeModal()
          } else {
            // codigo que se ejecuta si no estoy editando
            sendPerson(values)
            resetForm()
          }
        }}
        validationSchema={Yup.object().shape({
          speak: Yup.string().required(),
          name: Yup.string().required(),
          estatura: Yup.number().required(),
          edad: Yup.number()
            // .test('len', 'Must be exactly 2 characters', val => val && val.toString().length === 2 )
            .min(0)
            .max(99),
        })}
      >
        {(errors) => (
          <Form>
            <div className='MyForm'>
              <label htmlFor='name'>Nombre</label>
              <Field id="name" name="name" placeholder="nombre..." />
              {errors.name ? (<div>{errors.name}</div>) : null}
              <ErrorMessage name="name"/>

              <label htmlFor='speak'>Speak</label>
              <Field id="speak" name="speak" placeholder="mensaje..." />
              {errors.speak ? (<div>{errors.speak}</div>) : null}
              <ErrorMessage name="speak"/>

              <label htmlFor='edad'>Edad</label>
              <Field id="edad" name="edad" placeholder="edad..." />
              {errors.edad ? (<div>{errors.edad}</div>) : null}
              <ErrorMessage name="edad"/>

              <label htmlFor='estatura'>Estatura</label>
              <Field id="estatura" name="estatura" placeholder="estatura..." />
              {errors.estatura ? (<div>{errors.estatura}</div>) : null}
              <ErrorMessage name="estatura"/>

              <button type='submit'>{isEditForm ? 'Editar' : 'Enviar'}</button>
            </div>
        </Form>
        )}
      </Formik>  
    </div>
  )
}

export default AddPeopleForm