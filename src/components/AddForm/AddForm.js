import { ErrorMessage, Field, Form, Formik } from 'formik';
import React from 'react';
import './AddForm.css';
import * as Yup from 'yup';
import axios from 'axios';

const AddForm = ({ isEditForm, film, closeModal, onUpdate }) => {

  const editMovie = async (values) => {
    try {
      const res = await axios.put(`https://hellowworldapi.azurewebsites.net/Movie/${film.id}`, values)
      if (res && res.status === 200) {
        // volver a cargar la lista
        onUpdate()
      }
    } catch (error) {
        console.error(error)
    }
  }
  
  const sendMovie = async (values) => {
    try {
      const res = await axios.post('https://hellowworldapi.azurewebsites.net/Movie', values)
      if (res && res.status === 200) {
        console.log('RESP DEL POST', res)
      }
    } catch (error) {
        console.error(error)
    }
  }

  return (
    <div className="Container">
      <h3>{isEditForm ? 'Editar pelicula' : 'Cargar pelicula'}</h3>  
      <Formik
        initialValues={{
          title: film?.title || '',
          genre: film?.genre || '',
          year: film?.year || '',
        }}
        onSubmit={async (values, { resetForm }) => {
          if (isEditForm) {
            // codigo que se ejecuta si estoy editando
            editMovie(values)
            closeModal()
          } else {
            // codigo que se ejecuta si no estoy editando
            sendMovie(values)
            resetForm()
          }
        }}
        validationSchema={Yup.object().shape({
          title: Yup.string().required(),
          genre: Yup.string().required(),
          year: Yup.number()
            .test('len', 'Must be exactly 4 characters', val => val && val.toString().length === 4 )
            .min(1900)
            .max(new Date().getFullYear()),
        })}
      >
        {(errors) => (
          <Form>
            <div className='MyForm'>
              <label htmlFor='title'>Titulo</label>
              <Field id="title" name="title" placeholder="titulo..." />
              {errors.title ? (<div>{errors.title}</div>) : null}
              <ErrorMessage name="title"/>

              <label htmlFor='genre'>Genero</label>
              <Field id="genre" name="genre" placeholder="genero..." />
              {errors.genre ? (<div>{errors.genre}</div>) : null}
              <ErrorMessage name="genre"/>

              <label htmlFor='year'>Año</label>
              <Field id="year" name="year" placeholder="año..." />
              {errors.year ? (<div>{errors.year}</div>) : null}
              <ErrorMessage name="year"/>

              <button type='submit'>{isEditForm ? 'Editar' : 'Enviar'}</button>
            </div>
        </Form>
        )}
      </Formik>  
    </div>
  )
}

export default AddForm