import axios from "axios";
import { useState } from "react";
import "./Card.module.css";
import Modal from 'react-modal';
import AddForm from "../AddForm/AddForm";

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    color: 'black',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    background: '#282c34',
  },
};

export default function Card({ film, onUpdate }) {
  const [modalIsOpen, setIsOpen] = useState(false)

  const deleteFilm = async () => {
    try {
      const res = await axios.delete(`https://hellowworldapi.azurewebsites.net/Movie/${film.id}`)
      if (res && res.status === 200) {
        console.log('RESP', res)
        onUpdate()
      }
    } catch (error) {
        console.error(error)
    }
  }

  return (
    <div className="Card">
      <span>Id: {film.id}</span>
      <p>Titulo: {film.title}</p>
      <p>Año: {film.year}</p>
      <p>Genero:{film.genre}</p>
      <button type="button" onClick={() => setIsOpen(true)}>Editar</button>
      <button type="button" onClick={() => deleteFilm()}>Borrar</button>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={() => setIsOpen(false)}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <button onClick={() => setIsOpen(false)}>Close</button>
        <AddForm
          isEditForm={true}
          film={film}
          closeModal={() => setIsOpen(false)}
          onUpdate={onUpdate}
        />
      </Modal>
    </div>
  );
}
