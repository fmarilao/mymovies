import axios from 'axios'
import React, { useEffect, useState } from 'react'
import PersonCard from '../PersonCard/PersonCard'
import './ListPersons.css'

const ListPersons = () => {
    const [results, setResults] = useState([]); 

    const getPersons = async () => {
        try {
            const res = await axios.get('https://hellowworldapi.azurewebsites.net/Person')
            if (res) {
              setResults(res.data)
            }
        } catch (error) {
            console.error(error)
        }
    }

    useEffect(() => {
      if (results.length === 0) {
        getPersons()
      }
    }, [])

  return (
    <div className="Container">
        <h3>
         Listado de Personas
        </h3>
        {results?.map((r, i) => {
        return (
          <div className="MyCard" key={i}>
            <PersonCard person={r} onUpdate={getPersons}/>
          </div>
        );
        })}
    </div>
  )
}

export default ListPersons