import axios from 'axios'
import React, { useEffect, useState } from 'react'
import Card from '../Card/Card'
import './ListMovies.css'

const ListMovies = () => {
    const [results, setResults] = useState([]); 

    const getMovies = async () => {
        try {
            const res = await axios.get('https://hellowworldapi.azurewebsites.net/Movie')
            if (res) {
              setResults(res.data)
            }
        } catch (error) {
            console.error(error)
        }
    }

    useEffect(() => {
      if (results.length === 0) {
          getMovies()
      }
    }, [])

  return (
    <div className="Container">
        <h3>
         Listado de peliculas
        </h3>
        {results?.map((r, i) => {
        return (
          <div className="MyCard" key={i}>
            <Card film={r} onUpdate={getMovies}/>
          </div>
        );
        })}
    </div>
  )
}

export default ListMovies