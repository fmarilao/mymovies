import { BrowserRouter, Link, Navigate, Route, Routes } from 'react-router-dom';
import './App.css';
import About from './components/About/About';
import AddForm from './components/AddForm/AddForm';
import AddPeopleForm from './components/AddPeopleForm/AddPeopleForm';
import HomePage from './components/HomePage/HomePage';
import ListMovies from './components/ListMovies/ListMovies';
import ListPersons from './components/ListPersons/ListPersons';
import PageNotFound from './components/PageNotFound/PageNotFound';

function App() {
  return (
    <BrowserRouter>
      <div className="Navbar">
        <Link to="/" className='link'>Home</Link>
        <Link to="/addmovie" className='link'>Add Movie</Link>
        <Link to="/listmovies" className='link'>List Movies</Link>
        <Link to="/addperson" className='link'>Add Person</Link>
        <Link to="/listpersons" className='link'>List Persons</Link>
        <Link to="/about" className='link'>About</Link>
      </div>
      <Routes>
        <Route path='/' element={<HomePage />} />
        <Route path='/404' element={<PageNotFound />} />
        <Route path='/addmovie' element={<AddForm />} />
        <Route path='/listmovies' element={<ListMovies />} />
        <Route path='/addperson' element={<AddPeopleForm />} />
        <Route path='/listpersons' element={<ListPersons />} />
        <Route path='/about' element={<About />} />
        <Route path='*' element={<Navigate replace to='/404' />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
